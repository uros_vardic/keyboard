#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "utils.h"

#define SHIFT_DOWN  200
#define CTRL_DOWN   201
#define ALT_DOWN    202
#define SHIFT_UP    300
#define CTRL_UP     301
#define ALT_UP 	    302

#define BUFFER_SIZE 128
#define SC_COL_SIZE 2
#define SC_ROW_SIZE 128
#define MN_COL_SIZE 16
#define MN_ROW_SIZE 66

static int scancodes[SC_COL_SIZE][SC_ROW_SIZE];
static int mnemonics[MN_COL_SIZE][MN_ROW_SIZE];

void load_scancodes(const char *scancodes_filename) {
	int fd = open(scancodes_filename, O_RDONLY);

	if (fd == -1) {
		printerr("Error while opening file: ");
		printerr(scancodes_filename);
		_exit(1);
	}

	char buffer[BUFFER_SIZE];

	int i, j, row_length;

	for (i = 0; i < SC_COL_SIZE; i++) {
		row_length = fgets(buffer, BUFFER_SIZE, fd);

		for (j = 0; j <	row_length; j++)
			scancodes[i][j] = buffer[j];
	}

	close(fd);	
}

void print_scancodes() {
	char buffer[BUFFER_SIZE];

	int i, j;
	
	for (i = 0; i < SC_COL_SIZE; i++) {
		for (j = 0; j < SC_ROW_SIZE; j++) 
			buffer[j] = scancodes[i][j];

		write(1, buffer, SC_ROW_SIZE);
	}
}

int is_registered(char scancode) {
	int i, j;

	for (i = 0; i < SC_COL_SIZE; i++)
		for (j = 0; j < SC_ROW_SIZE; j++)
			if (scancode == scancodes[i][j])
				return 1;

	return 0;
}

void load_mnemonics(const char *mnemonic_filename) {
	int fd = open(mnemonic_filename, O_RDONLY);

	if (fd == -1) {
		printerr("Error while opening file: ");
		printerr(mnemonic_filename);
		_exit(1);
	} 

	char buffer[BUFFER_SIZE];

	fgets(buffer, BUFFER_SIZE, fd);

	int col_length = atoi(buffer);

	if (col_length > MN_COL_SIZE) {
		printerr("Too many mnemonics in file: ")
		printerr(mnemonic_filename);
		_exit(1);
	}

	int i, j, row_length;

	for (i = 0; i < col_length; i++) {
		row_length = fgets(buffer, BUFFER_SIZE, fd);

		if (row_length > MN_ROW_SIZE) {
			printerr("Mnemonic is too long and will be skipped");
			continue;
		}

		for (j = 0; j < row_length; j++) {
			// skip the mnemonic if he isn't registered
			if (j == 0 && is_registered(buffer[0]) != 1) {
				printerr("Mnemonic isn't registered and will be skipped");
				continue;
			}

			mnemonics[i][j] = buffer[j];
		}
	}

	close(fd);
}

void print_mnemonics() {
	char buffer[BUFFER_SIZE];

	int i, j;

	for (i = 0; i < MN_COL_SIZE; i++) {
		for (j = 0; j < MN_ROW_SIZE; j++)
			buffer[j] = mnemonics[i][j];

		write(1, buffer, MN_ROW_SIZE);
	}
}

void load_config(const char *scancodes_filename, const char *mnemonic_filename) {
	load_scancodes(scancodes_filename);	
	load_mnemonics(mnemonic_filename);
	// print_scancodes();
	// print_mnemonics();
}

int ctrl_flag = 0, shift_flag = 0, alt_flag = 0, ascii_code = 0;

int shift_down = SHIFT_DOWN, shift_up = SHIFT_UP;

int alt_down = ALT_DOWN, alt_up = ALT_UP;

int ctrl_down = CTRL_DOWN, ctrl_up = CTRL_UP;

int process_scancode(int scancode, char *buffer) {
	int result = 0;

	// ==========================
	// *******SHIFT FLAG*********
	// ==========================
	__asm__ __volatile__ (
		"SHIFT_FLAG:"
		"cmpl %%ebx, shift_down;"
		"je SHIFT_FLAG_DOWN;"
		"cmpl %%ebx, shift_up;"
		"je SHIFT_FLAG_UP;"

		"jmp CTRL_FLAG;"

		"SHIFT_FLAG_DOWN:"
		"movl $1, shift_flag;"
		"xorl %%ecx, %%ecx;"
		"jmp EXIT;"

		"SHIFT_FLAG_UP:"
		"movl $0, shift_flag;"
		"xorl %%ecx, %%ecx;"
		"jmp EXIT;"
		: 
		: "b" (scancode)
		: 
	);

	// ==========================
	// ********CTRL FLAG*********
	// ==========================
	__asm__ __volatile__ (
		"CTRL_FLAG:"
		"cmpl %%ebx, ctrl_down;"
		"je CTRL_FLAG_DOWN;"
		"cmpl %%ebx, ctrl_up;"
		"je CTRL_UP_FLAG;"

		"jmp ALT_FLAG;"

		"CTRL_FLAG_DOWN:"
		"movl $1, ctrl_flag;"
		"xorl %%ecx, %%ecx;"
		"jmp EXIT;"

		"CTRL_UP_FLAG:"
		"movl $0, ctrl_flag;"
		"xorl %%ecx, %%ecx;"
		"jmp EXIT;"
		:
		: "b" (scancode)
		:
	);

	// ==========================
	// ********ALT FLAG**********
	// ==========================
	__asm__ __volatile__ (
		"ALT_FLAG:"
		"cmpl %%ebx, alt_down;"
		"je ALT_FLAG_DOWN;"
		"cmpl %%ebx, alt_up;"
		"je ALT_FLAG_UP;"

		"jmp STATE;"

		"ALT_FLAG_DOWN:"
		"movl $1, alt_flag;"
		"movl $0, ascii_code;"
		"xorl %%ecx, %%ecx;"
		"jmp EXIT_ALT_FLAG;"

		"ALT_FLAG_UP:"
		"xorl %%ecx, %%ecx;"
		"movl ascii_code, %%eax;"
		"movl 12(%%ebp), %%ebx;"
		"movl %%eax, (%%ebx);"
		"movl $0, alt_flag;"
		"movl $0, ascii_code;"
		"incl %%ecx;"

		"EXIT_ALT_FLAG:"
		:
		: "b" (scancode)
		: "%eax" ,"memory"
	);

	__asm__ __volatile__ ("jmp EXIT;" : : : );

	// ==========================
	// **********STATES**********
	// ==========================
	__asm__ __volatile__ (
		"STATE:"
		"cmpl $1, shift_flag;"
		"je SHIFT_DOWN_STATE;"

		"cmpl $1, ctrl_flag;"
		"je CTRL_DOWN_STATE;"

		"cmpl $1, alt_flag;"
		"je ALT_DOWN_STATE;"

		"jmp DEFAULT_STATE;"

		// ==========================
		// **********SHIFT***********
		// ==========================
		"SHIFT_DOWN_STATE:"
		// shift + ctrl state
		"cmpl $1, ctrl_flag;"
		"jne .NORMAL;"
		"xorl %%ecx, %%ecx;"
		"xorl %%edx, %%edx;"
		"addl $128, %%ebx;"
		"pushl %%ebp;"
		"pushl %%esi;"
		"leal scancodes, %%ebp;"
		"leal mnemonics, %%esi;"
		"jmp INDEX_LOOP;"

		// shift + alt state
		"cmpl $1, alt_flag;"
		"jne .NORMAL;"
		"addl $128, %%ebx;"
		"jmp ALT_DOWN_STATE;"

		".NORMAL:"
		"xorl %%ecx, %%ecx;"
		"pushl %%ebp;"
		"leal scancodes, %%ebp;"
		"addl $128, %%ebx;"
		"movl (%%ebp, %%ebx, 4), %%eax;"
		"popl %%ebp;"
		"movl 12(%%ebp), %%ebx;"
		"movl %%eax, (%%ebx);" 
		"incl %%ecx;"
		"jmp EXIT_STATE;"

		// ==========================
		// ***********CTRL***********
		// ==========================
		"CTRL_DOWN_STATE:"
		// setup for index loop
		"xorl %%ecx, %%ecx;"
		"xorl %%edx, %%edx;"
		"pushl %%ebp;"
		"pushl %%esi;"
		"leal scancodes, %%ebp;"
		"leal mnemonics, %%esi;"

		"INDEX_LOOP:"
		"movl (%%ebp, %%ebx, 4), %%eax;"
		"pushl %%ebx;"
		"movl %%edx, %%ebx;"
		"imull $66, %%ebx;"
		"movl (%%esi, %%ebx, 4), %%ecx;"
		"popl %%ebx;"
		"incl %%edx;"
		"cmpl $16, %%edx;"
		"je EXIT_INDEX_LOOP;"
		"cmpl %%eax, %%ecx;"
		"jne INDEX_LOOP;"

		"EXIT_INDEX_LOOP:" 
		"subl $1, %%edx;"
		"popl %%ebp;"
		"popl %%esi;"

		// setup for mnemonic loop
		"leal %0, %%ebp;"
		"leal mnemonics, %%esi;"
		"xorl %%ecx, %%ecx;"
		"imull $66, %%edx, %%ebx;"
		"xorl %%edx, %%edx;"
		// skip space
		"incl %%ebx;"
		"jmp .CONDITION;"

		"MNEMONIC_LOOP:"
		"incl %%ebx;"
		"movl (%%esi, %%ebx, 4), %%edx;"
		"movl %%ecx, %%eax;"
		"addl 12(%%ebp), %%eax;"
		"movl %%edx, (%%eax);"
		"incl %%ecx;"

		".CONDITION:"
		"cmpl $10, %%edx;"
		"je EXIT_STATE;"
		"cmpl $64, %%ecx;"
		"jle MNEMONIC_LOOP;"
		"jmp EXIT_STATE;"

		// ==========================
		// ***********ALT************
		// ==========================
		"ALT_DOWN_STATE:"
		"pushl %%ebp;"
		"leal scancodes, %%ebp;"
		"movl (%%ebp, %%ebx, 4), %%eax;"
		"subl $48, %%eax;"
		"imull $10, ascii_code, %%ecx;"
		"addl %%ecx, %%eax;"
		"movl %%eax, ascii_code;"
		"xorl %%ecx, %%ecx;"
		"popl %%ebp;"
		"jmp EXIT_STATE;"

		// ==========================
		// **********DEFAULT*********
		// ==========================
		"DEFAULT_STATE:"
		"xorl %%ecx, %%ecx;"
		"pushl %%ebp;"
		"leal scancodes, %%ebp;"
		"movl (%%ebp, %%ebx, 4), %%eax;"
		"popl %%ebp;"
		"movl 12(%%ebp), %%ebx;"
		"movl %%eax, (%%ebx);"
		"incl %%ecx;"
		"EXIT_STATE:"
		: "+m" (*buffer)
		: "b" (scancode)
		: "%eax", "%ecx", "%edx", "memory"
	);

	__asm__ __volatile__ ("EXIT:" : "=c" (result) : : );

	return result;
}