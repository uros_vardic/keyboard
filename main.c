#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "scan.h"

#define UTIL_IMPLEMENTATION
#include "utils.h"

#define EOF			400
#define BUFFER_SIZE 128

void read_scancodes(const char *file_name) {
	int fd = open(file_name, O_RDONLY);

	if (fd == -1) {
		printerr("Error while opening file: ");
		printerr(file_name);
		return;
	}

	char buffer[BUFFER_SIZE];

	fgets(buffer, BUFFER_SIZE, fd);

	char printBuffer[BUFFER_SIZE];

	int scancode = atoi(buffer), output_size;

	int counter = 0;

	while (scancode != EOF) {
		output_size = process_scancode(scancode, buffer);
		write(1, buffer, output_size);
		fgets(buffer, BUFFER_SIZE, fd);
		scancode = atoi(buffer);
	}
}

void trim(char *s) {
	char *p = s;

	while (*s != '\n')
		*p++ = *s++;

	*p = '\0';
}

int main(int argc, char *argv[]) {
	write(1, "Loading configurations...\n", strlen("Loading configurations...\n"));
	load_config("scancodes.tbl", "ctrl.map");

	char buffer[BUFFER_SIZE];

	write(1, "Enter scancode file name: ", strlen("Enter scancode file name: "));

	int input_length = read(0, buffer, BUFFER_SIZE);

	trim(buffer);

	while (strncmp(buffer, "exit", input_length) != 0) {
		read_scancodes(buffer);
		write(1, "Enter scancode file name: ", strlen("Enter scancode file name: "));
		input_length = read(0, buffer, BUFFER_SIZE);
		trim(buffer);
	}

	_exit(0);
}
